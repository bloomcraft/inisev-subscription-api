<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Models\User;
use App\Notifications\SendPostsToSubscribersNotification;

class SendPostsToSubscribers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribers:send-posts {website_id} {post_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send posts to subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->confirm('Are you sure you want to send posts to subscribers?')) {
            $this->info('Command cancelled.');
            return 0;
        }

        $website_id = $this->argument('website_id');
        $post_id = $this->argument('post_id');

        $post = \App\Models\Post::find($post_id);
        $website = \App\Models\Website::find($website_id);

        $website->subscriptions->pluck('user_id')->each(function($user_id) use ($post) {
            $user = User::find($user_id);
            $user->notify(new SendPostsToSubscribersNotification($post));
        });

        // alternative way to send notifications
        // $subscribers = $website->subscriptions->pluck('user_id');
        // foreach ($subscribers as $subscriber_id) {
        //     $subscriber = User::find($subscriber_id);
        //     $subscriber->notify(new SendPostsToSubscribersNotification($post));
        // }

        $this->info('Subscribers notified!');
    }
}
