<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return response()->json($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'website_id' => ['required', 'integer'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) return response()->json($validator->errors(), 400);

        // validate if the website exists
        $website = Website::find($request->website_id);
        if (!$website) return response()->json(['message' => 'Website not found'], 404);

        $post = Post::create($request->all());

        if ($post) {
            /** send email to website subscribers using laravel job **/
            $this->dispatch(new \App\Jobs\SendPostsToSubscribers([
                'website_id' => $website->id,
                'post_id' => $post->id
            ]));

            /** send email to website subscribers using artisan command **/
            // Artisan::queue('subscribers:send-posts', ['website_id' => $website->id, 'post_id' => $post->id]);

            /** send email to website subscribers normally **/
            // foreach ($post->website->subscriptions as $subscriber) {
            //     $subscriber->notify(new \App\Notifications\SendPostsToSubscribersNotification($post));
            // }

            return response()->json($post, 201);
        }
        
        return response()->json(['message' => 'Post not created'], 400);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return response()->json($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Post::find($id);
        $post->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
    }
}
