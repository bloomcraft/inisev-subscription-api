<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPostsToSubscribers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $website_id = $this->data['website_id'];
        $post_id = $this->data['post_id'];

        $post = \App\Models\Post::find($post_id);
        $website = \App\Models\Website::find($website_id);

        if (!$website || !$post) return;

        $website->subscriptions->pluck('user_id')->each(function($user_id) use ($post) {
            $user = \App\Models\User::find($user_id);
            $user->notify(new \App\Notifications\SendPostsToSubscribersNotification($post));
        });
    }
}
