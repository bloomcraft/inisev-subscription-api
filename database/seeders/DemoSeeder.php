<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use Faker\Factory;

use App\Models\User;
use App\Models\Website;
use App\Models\Post;

class DemoSiteSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate previous data
        echo 'deleting old data.....';
        $this->deletePreviousData();

        echo PHP_EOL , 'seeding websites...';
        $this->websitesData();

        echo PHP_EOL , 'seeding posts...';
        $this->postsData();

        echo PHP_EOL , 'seeding users...';
        $this->usersData();

        echo PHP_EOL , 'seeding completed.';

    }


    private function deletePreviousData()
    {
        /***
         * This code is MYSQL specific
        */
        DB::statement("SET foreign_key_checks=0");
        
        // truncate relations
        DB::table('websites')->truncate();
        DB::table('posts')->truncate();
        DB::table('users')->truncate();
        DB::table('subscriptions')->truncate();

        DB::statement("SET foreign_key_checks=1");
    }

    private function websitesData()
    {
        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\Internet($faker));

        for($i = 0; $i < 10; $i++) {
            Website::create([
                'name' => $faker->domainName,
                'url' => $faker->url,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }

    private function postsData()
    {
        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\Lorem($faker));
        $faker->addProvider(new \Faker\Provider\DateTime($faker));

        $websites = Website::all();

        foreach($websites as $website) {
            for($i = 0; $i < 10; $i++) {
                $post = new Post();
                $post->title = $faker->sentence(5);
                $post->description = $faker->paragraph(1);
                $post->content = $faker->paragraph(10);
                $post->created_at = Carbon::now();
                $post->updated_at = Carbon::now();
                $post->website_id = $website->id;
                $post->save();
            }
        }
    }

    private function usersData()
    {
        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_US\Address($faker));
        $faker->addProvider(new \Faker\Provider\en_US\PhoneNumber($faker));
        $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new \Faker\Provider\en_US\Payment($faker));
        $faker->addProvider(new \Faker\Provider\DateTime($faker));
        $faker->addProvider(new \Faker\Provider\Lorem($faker));
        $faker->addProvider(new \Faker\Provider\Internet($faker));

        for($i = 0; $i < 5; $i++) {
            User::create([
                'email' => $faker->unique()->safeEmail
            ]);
        }
    }
}
