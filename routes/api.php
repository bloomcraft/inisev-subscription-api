<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*****************************
 * Website API
*****************************/
Route::get('/websites', 'WebsiteController@index');

// START: provide these query params: name, url
Route::post('/websites', 'WebsiteController@store'); // https://testipordomain.com/websites/?name=test&url=test.com
Route::put('/websites/{website_id}', 'WebsiteController@update'); // https://testipordomain.com/websites/1/?name=test&url=test.com
// END: provide these query params: name, url

Route::delete('/websites/{website_id}', 'WebsiteController@destroy');
Route::get('/websites/{website_id}', 'WebsiteController@show');

/*****************************
 * Posts API
*****************************/
Route::get('/posts', 'PostController@index');

// START: provide these query params: website_id, title, description, content.
Route::post('/posts', 'PostController@store'); // https://testipordomain.com/posts/?website_id=1&title=new title&description=new description&content=new content
Route::put('/posts/{post_id}', 'PostController@update'); // https://testipordomain.com/posts/1/?website_id=1&title=new title&description=new description&content=new content
// END: provide these query params: website_id, title, description, content.

Route::delete('/posts/{post_id}', 'PostController@destroy');
Route::get('/posts/{post_id}', 'PostController@show');

/*****************************
 * Subscription API
*****************************/
Route::post('/subscribe/{email}/{website_id}', 'SubscriptionController@subscribe'); // https://testipordomain.com/subscribe/johndoe@gmail.com/1
Route::post('/unsubscribe/{email}/{website_id}', 'SubscriptionController@unsubscribe'); // https://testipordomain.com/unsubscribe/johndoe@gmail.com/1
